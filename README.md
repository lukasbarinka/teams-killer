# teams-killer

<img src="teams-killer.png"  width="120" height="120" style="float: left">
Script to kill MS Teams on Gnome Desktop. This project contain a script and
Gnome Desktop icon to run the script.

## Getting started

Clone repository and run install script:

```
git clone git@gitlab.com:lukasbarinka/teams-killer.git
teams-killer/install.sh
```


## Installation

Installation script will:

1. **Modify and copy `teams-killer.desktop` configuration file** into `~/.local/share/applications/teams-killer.desktop`. The desktop file points to `teams-killer` script and `teams-killer.png` icon.
1. **Add diretory containing `teams-killer` script** to PATH variable in
profile files (`~/.bash_profile`, `~/.bash_login` or `~/.profile`).
1. **Remove `install.sh` script** when successfuly installed.
