#!/bin/bash

desktop_location=~/.local/share/applications/teams-killer.desktop

err() {
  local msg
  local IFS=$'\n'
  for msg in $*; do
    printf '%s[err]: %s\n' "${0}" "${msg}" >&2
  done
  exit 2
}
verbose() {
  (( VERBOSE )) || return
  local msg
  local IFS=$'\n'
  for msg in $*; do
    printf '%s[info]: %s\n' "${0}" "${msg}" >&2
  done
}

usage() {
  cat <<USAGE
Usage:  $0 [-vh]

        -h  this help
        -v  verbose
USAGE
}

modify() {
  local conf=${1}
  verbose "Testing config file: '${conf}'"
  [[ -f ${conf} ]] || return 1
  if ! grep -qF "PATH=\$PATH:${install_dir}" "${conf}"; then
    verbose "Adding '${install_dir}' to PATH in '${conf}' file"
    printf '%s\n' "PATH=\$PATH:${install_dir}" >> "${conf}"
  else
    verbose "Directory '${install_dir}' already part of PATH in '${conf}' file"
  fi
  return 0
}

while getopts vh opt; do
  case ${opt} in
    v) (( VERBOSE++ ));;
    h) usage; exit 0;;
    \?) usage >&2; exit 2;;
  esac
done


# Get installation directory pathname
realpath=$( realpath "${0}" )
install_dir=${realpath%/*}
verbose "$( declare -p realpath install_dir )"


# Modify .desktop configuration file
desktop_file="${install_dir}/teams-killer.desktop"
[[ -f ${desktop_file} ]] || err "Desktop file '${desktop_file}' not found"
[[ -r ${desktop_file} ]] || err "Desktop file '${desktop_file}' not readable"
[[ -w ${desktop_file} ]] || err "Desktop file '${desktop_file}' not writable"
[[ -s ${desktop_file} ]] || err "Desktop file '${desktop_file}' is empty"
mkdir -p "${desktop_location%/*}"
sed "s~__INSTALL-PATH__~${install_dir}~g" \
  "${desktop_file}" > "${desktop_location}"


# Find profile file and extend PATH
if ! tr : '\n' <<<"$PATH" | grep -q -m1 "${install_dir}"; then
  for conf in ~/.bash_profile ~/.bash_login ~/.profile; do
    if modify "${conf}"; then
      modified=1
      break
    fi
  done
  (( modified )) || err \
    "No profile file found" \
    "Add: \`PATH=\$PATH:${install_dir}' to your profile file"
else
  verbose "Directory '${install_dir}' already part of PATH"
fi

rm "${realpath}"
